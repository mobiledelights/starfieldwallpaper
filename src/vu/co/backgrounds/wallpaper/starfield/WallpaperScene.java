package vu.co.backgrounds.wallpaper.starfield;
//star system

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;

import java.util.Random;

import vu.co.backgrounds.wallpaper.starfield.particle.Star;

public class WallpaperScene {

	private Context context = null;
	private Bitmap backgroundBitmap = null;
	private Bitmap resizedBitmap = null;
	private Star[] flyingStarsArray = new Star[150];
	private Star[] staticStarsArray = new Star[60];
	
	private SharedPreferences prefs = null;
	private PreferenceListener listener = null;
	
	private int width = 320;
	private int height = 480;
	
	public WallpaperScene(Context context) {
		this.context = context;
		
		listener = new PreferenceListener();
		
		prefs = context.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
		prefs.registerOnSharedPreferenceChangeListener(listener);

		listener.onSharedPreferenceChanged(prefs, PreferenceActivity.WALLPAPER_NUMBER);
	}
	
	public void drawStars(Canvas canvas) {
		for (int i = 0; i < flyingStarsArray.length; i++) {
			flyingStarsArray[i].update(canvas);
			flyingStarsArray[i].draw(canvas);
		}

		for (int i = 0; i < staticStarsArray.length; i++) {
			staticStarsArray[i].update(canvas);
			staticStarsArray[i].draw(canvas);
		}
	}
	
	public void drawBackground(Canvas canvas) {
		canvas.drawBitmap(resizedBitmap, 0, 0, null);
	}

	public void drawPlanet() {
		
	}

	public void resize(int width, int height) {
		this.width = width;
		this.height = height;

		resizedBitmap = Bitmap.createScaledBitmap(backgroundBitmap, width, height, false);

		Bitmap star = BitmapFactory.decodeResource(this.context.getResources(), R.drawable.star2x2);
		Bitmap starBig = BitmapFactory.decodeResource(this.context.getResources(), R.drawable.star_big);
		Bitmap starSmall = BitmapFactory.decodeResource(this.context.getResources(), R.drawable.star_small);

		
		Random random = new Random();
		for (int i = 0; i < flyingStarsArray.length; i++) {
			flyingStarsArray[i] = new Star(random.nextInt(width/10)-width/20 + width/2
					, random.nextInt(height/20)-height/40 + height/2,
					/*random.nextFloat()*/0.1f, width, height, false, star);
		}

		for (int i = 0; i < staticStarsArray.length; i++) {
			Bitmap use;
			if (random.nextInt(2) == 0) {
				use = starBig;
			} else {
				use = starSmall;
			}
			staticStarsArray[i] = new Star(random.nextInt(width), random.nextInt(height), random.nextInt(10) + 1, width, height, true, use);
		}
	}
	
	private class PreferenceListener implements OnSharedPreferenceChangeListener {

		@Override
		public void onSharedPreferenceChanged(
				SharedPreferences sharedPreferences, String key) {
			if (key.equals(PreferenceActivity.WALLPAPER_NUMBER)) {
				switch (sharedPreferences.getInt(PreferenceActivity.WALLPAPER_NUMBER, 0)) {
				case 0:
					backgroundBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.dusty_spiral_in_virgo);
					break;
				case 1:
					backgroundBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.aurora);
					break;
				case 2:
					backgroundBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.erth_view_from_space);
					break;
				case 3:
					backgroundBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.illusion_cosmic_clouds);
					break;
				case 4:
					backgroundBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.large_magnetic_cloud);
					break;
				case 5:
					backgroundBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.looks_at_light_and_dark_in_the_universe);
					break;
				case 6:
					backgroundBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.perseus);
					break;
				case 7:
					backgroundBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.sun_ultraviolet);
					break;
				case 8:
					backgroundBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.sunrise_from_international_spacestation);
					break;
				case 9:
					backgroundBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.the_odd_trio);
					break;
				default:
					break;
				}
				resizedBitmap = Bitmap.createScaledBitmap(backgroundBitmap, width, height, false);
			}
		}
	}
	
}
