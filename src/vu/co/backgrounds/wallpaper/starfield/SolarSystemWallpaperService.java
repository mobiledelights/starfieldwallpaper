package vu.co.backgrounds.wallpaper.starfield;

import android.graphics.Canvas;
import android.graphics.Color;
import android.os.Handler;
import android.service.wallpaper.WallpaperService;
import android.view.SurfaceHolder;

public class SolarSystemWallpaperService extends WallpaperService {
	
	private WallpaperScene scene = null;
	
	@Override
	public void onCreate() {
		super.onCreate();
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
	}

	@Override
	public Engine onCreateEngine() {
		return new SolarSystemWallpaperEngine();
	}
	
	private class SolarSystemWallpaperEngine extends Engine {
		
		private SurfaceHolder holder = getSurfaceHolder();
		private Canvas canvas = holder.lockCanvas();
		
		private final Handler handler = new Handler();
		private final Runnable drawRunner = new Runnable() {
			
			@Override
			public void run() {
				draw();
			}
		};
		
		private boolean isVisible;
		
		public SolarSystemWallpaperEngine() {
			scene = new WallpaperScene(getApplicationContext());
		}
		
		@Override
		public void onCreate(SurfaceHolder surfaceHolder) {
			super.onCreate(surfaceHolder);
		}
		
		@Override
		public void onDestroy() {
			super.onDestroy();
			handler.removeCallbacks(drawRunner);
		}
		
		@Override
		public void onVisibilityChanged(boolean visible) {
			super.onVisibilityChanged(visible);
			this.isVisible = visible;
			
			if (isVisible) {
				draw();
			} else {
				handler.removeCallbacks(drawRunner);
			}
		}
		
		@Override
		public void onSurfaceCreated(SurfaceHolder holder) {
			super.onSurfaceCreated(holder);
		}

		@Override
		public void onSurfaceDestroyed(SurfaceHolder holder) {
			super.onSurfaceDestroyed(holder);
			
		}
		
		@Override
		public void onSurfaceChanged(SurfaceHolder holder, int format,
				int width, int height) {
			if (scene != null)
				scene.resize(width, height);
			
			super.onSurfaceChanged(holder, format, width, height);
		}
		
		@Override
		public void onOffsetsChanged(float xOffset, float yOffset,
				float xOffsetStep, float yOffsetStep, int xPixelOffset,
				int yPixelOffset) {
			draw();
		}
		
		private void draw() {
			
			try {
				canvas = holder.lockCanvas();
				canvas.drawColor(Color.BLACK);
				
				if (canvas != null) {
					scene.drawBackground(canvas);
					scene.drawStars(canvas);
				}
			}
			finally {
				if (canvas != null) {
					holder.unlockCanvasAndPost(canvas);
				}
			}
			
			handler.removeCallbacks(drawRunner);
			handler.postDelayed(drawRunner, 1000 / 50);
		}
	}
	
}
