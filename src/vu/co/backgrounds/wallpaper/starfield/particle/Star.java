package vu.co.backgrounds.wallpaper.starfield.particle;

import java.util.Random;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;

public class Star {

	private float x = 0;
	private float y = 0;
	private float velocity = 0.0f;
	private float velocityX = 0.0f;
	private float velocityY = 0.0f;
	private int width = 0;
	private int height = 0;
	private int alpha = 0;
	private int angle = 0;
	private boolean blinking = false;
	private Bitmap starBitmap = null;
	private Paint paint = null;
	
	//for blinking star velocity equals blinking speed

	public Star(int x, int y, float velocity, int widht, int height, boolean blinking, Bitmap starBitmap) {
		this.x = x;
		this.y = y;
		this.width = widht;
		this.height = height;
		this.velocity = velocity;
		this.blinking = blinking;
		this.velocityX = (x - widht / 2) * velocity;
		this.velocityY = (y - height / 2) * velocity;
		this.starBitmap = starBitmap;
		this.alpha = random.nextInt(256);
		this.angle = random.nextInt(360);
		this.paint = new Paint();
	}

	public Bitmap getStarBitmap() {
		return starBitmap;
	}

	public float getX() {
		return x;
	}

	public void setX(float x) {
		this.x = x;
	}

	public float getY() {
		return y;
	}

	public void setY(float y) {
		this.y = y;
	}

	public void draw(Canvas canvas) {
		canvas.save();
		if (blinking) {
			paint.setAlpha(alpha);
			canvas.rotate(angle, getX(), getY());
		} else {
			float relativeDistance = ((x - width / 2) * (x - width / 2) + (y - height / 2) * (y - height / 2)) * 4 / (height * height);
			int alpha = (int)(relativeDistance * 255 * 1.5);
			if (alpha > 255)
				alpha = 255;
			paint.setAlpha(alpha);
		}
		canvas.drawBitmap(getStarBitmap(), getX(), getY(), paint);
		canvas.restore();
	}

	private Random random = new Random();
	
	public void update(Canvas canvas) {
		if (blinking) {
			alpha -= velocity;
			if (alpha < 0) {
				alpha = 0;
				velocity = -velocity;
			} 
			if (alpha > 255) {
				alpha = 255;
				velocity = -velocity;
			}
		} else {
			x += velocityX;
			y += velocityY;
			if (x > width || x < 0 || y > height || y < 0) {
				x = random.nextInt(width/10)-width/20 + width/2;
				velocityX = (x - width / 2) * velocity;
				y = random.nextInt(height/20)-height/40 + height/2;
				velocityY = (y - height / 2) * velocity;
			}
		}
		
	}

}
