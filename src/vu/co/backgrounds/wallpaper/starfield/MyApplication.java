package vu.co.backgrounds.wallpaper.starfield;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

import java.util.List;

public class MyApplication extends Application {
	
	public static  boolean PLAY_MARKET = false; 
	public static  boolean AMAZON_MARKET = false;
	public static  boolean SAMSUNG_MARKET = false;
	public static  boolean OTHER_MARKET = false;
	
	private static MyApplication instance;
	
    @Override
    public void onCreate() {
      super.onCreate();  
      instance = this;
    }

    public static Context getContext() {
        return instance;
    }
    

	public String getPackage() {
		return MyApplication.class.getPackage().getName();
	}
	
	public void startMainActivity(Context ctx){
		Intent i = new Intent(ctx, PreferenceActivity.class);
        ctx.startActivity(i);
	}
	
	public void checkMarkets()
	{
	    PackageManager pm = getPackageManager();
	    String installationSource = pm.getInstallerPackageName(getPackage());
	    if (installationSource != null && installationSource.startsWith("com.amazon")) {
	    	//"com.amazon.venezia"
	    	AMAZON_MARKET = true;
	    }
	    if (installationSource != null && installationSource.startsWith("com.amazon")) {
	    	// "com.android.vending"
	    	PLAY_MARKET = true;
	    }
	}
	
	public boolean isApplicationIstalledByPackageName(String packageName) {
		List<PackageInfo> packages = getPackageManager()
				.getInstalledPackages(0);
		if (packages != null && packageName != null) {
			for (PackageInfo packageInfo : packages) {
				if (packageName.equals(packageInfo.packageName)) {
					return true;
				}
			}
		}
		return false;
	}
	
    
}
