package vu.co.backgrounds.wallpaper.starfield;

import android.app.Activity;
import android.app.WallpaperManager;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;
import com.startapp.android.publish.Ad;
import com.startapp.android.publish.AdDisplayListener;
import com.startapp.android.publish.AdEventListener;
import com.startapp.android.publish.StartAppAd;
import com.startapp.android.publish.StartAppAd.AdMode;
import com.startapp.android.publish.splash.SplashConfig;
import com.startapp.android.publish.splash.SplashHideListener;

public class PreferenceActivity extends Activity {
	
	public static final boolean SLIDEME_MARKET = false;
	
	public static final String WALLPAPER_NUMBER = "wallpaper_number";
	
	private static final String SPINNER_POSITION = "spinner_position";
	
	private Spinner backgroundSelectSpinner = null;
	private String[] backgroundsArray = null;
	
	private SharedPreferences prefs = null;
	
	public static boolean paused = true;
	private boolean adLoaded = false;
	
	//  Declarations used for showing full-screen interstitial
	protected StartAppAd startAppAd = new StartAppAd(this);
	protected AdEventListener adReady = new AdEventListener() {
		@Override
		public void onReceiveAd(Ad arg0) {
			adLoaded = true;
		}
		
		@Override
		public void onFailedToReceiveAd(Ad arg0) {
			adLoaded = false;
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		StartAppAd.init(this, getString(R.string.startapp_devid),
				getString(R.string.startapp_appid));
		
		setContentView(R.layout.preference_activity_layout);
		
		if (SLIDEME_MARKET) {
			View view = findViewById(R.id.rate_layout);
			if (view != null) {
				view.setVisibility(View.GONE);
			}
			
			view = findViewById(R.id.share_layout);
			if (view != null) {
				view.setVisibility(View.GONE);
			}
		}
		
		StartAppAd.showSplash(this, savedInstanceState, 
		  		new SplashConfig()
		  		.setTheme(SplashConfig.Theme.OCEAN)
		  		.setAppName(getString(R.string.app_name)) 
		  		.setLogo(R.drawable.icon)  // resource ID
		  		.setOrientation(SplashConfig.Orientation.AUTO),
		  		null, new SplashHideListener() {
		  			@Override
		  			public void splashHidden() {
		  				// TODO Initialize Airpush here (shows EULA dialog)?
		  				RatingHelper.app_launched(PreferenceActivity.this);
		  			}
		  		});
				
				// Request a full screen interstitial which will be shown on load 
				// android.util.base64 available? StartApp AppWall crashes otherwise
				if (android.os.Build.VERSION.SDK_INT >= 8) {
					startAppAd.loadAd(adReady);
				} else {
					startAppAd.loadAd(AdMode.FULLPAGE, adReady);
				}
		
		prefs = getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
		
		backgroundsArray = getResources().getStringArray(R.array.backgrounds_array);
		backgroundSelectSpinner = (Spinner) findViewById(R.id.stars_spinner);
		ArrayAdapter<String> wallpaperSpinnerArrayAdapter = new ArrayAdapter<String>
			(this, android.R.layout.simple_spinner_dropdown_item, backgroundsArray);
		
		backgroundSelectSpinner.setAdapter(wallpaperSpinnerArrayAdapter);
		backgroundSelectSpinner.setSelection(prefs.getInt(SPINNER_POSITION, 0), false);
		backgroundSelectSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				if (saveBackgroundChoiceToPrefs(position)) {
					Toast.makeText(
							PreferenceActivity.this,
							getText(R.string.successful_background_change),
							Toast.LENGTH_SHORT).show();
					switch (position) {
					case 0:
						saveBackgroundChoiceToPrefs(position);
						break;
					case 1:
						saveBackgroundChoiceToPrefs(position);
						break;
					case 2:
						saveBackgroundChoiceToPrefs(position);
						break;
					case 3:
						saveBackgroundChoiceToPrefs(position);
						break;
					case 4:
						saveBackgroundChoiceToPrefs(position);
						break;
					case 5:
						saveBackgroundChoiceToPrefs(position);
						break;
					case 6:
						saveBackgroundChoiceToPrefs(position);
						break;
					case 7:
						saveBackgroundChoiceToPrefs(position);
						break;
					case 8:
						saveBackgroundChoiceToPrefs(position);
						break;
					case 9:
						saveBackgroundChoiceToPrefs(position);
						break;
					default:
						break;
					}
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				
			}
		});
	}

	public static void openMarket(Context context, String packageName) {
		Intent marketIntent = new Intent(
				Intent.ACTION_VIEW,
				Uri.parse("market://" + packageName));
    	marketIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY
    			| Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
		try {
			context.startActivity(marketIntent);
		} catch (ActivityNotFoundException e) {
			marketIntent = new Intent(
					Intent.ACTION_VIEW,
					Uri	.parse("http://play.google.com/store/apps/"	+ packageName));
			context.startActivity(marketIntent);
		}
	}
	
	public void setAsWallpaper(View v) {
		Intent intent = new Intent();
		intent.setAction(WallpaperManager.ACTION_LIVE_WALLPAPER_CHOOSER);
		try {
			startActivity(intent);
			
			// Toast short text
			Toast.makeText(
					this,
					getString(R.string.setWallpaperPre) + " " +
					getString(R.string.app_name)
					+ " " + getString(R.string.setWallpaperPost),
					Toast.LENGTH_LONG).show();

		} catch (ActivityNotFoundException e) {
			Toast.makeText(
					this, "Sorry, Live Wallpapers NOT supported.",
					Toast.LENGTH_LONG).show();

		}
		
		// and finish this?
//		FireFliesWallpaperPreferencesActivity.this.finish();
	}
	
	private boolean adShown = false;
	@Override
	public void onBackPressed() {
		if (adShown || !adLoaded) {
			startAppAd.close();
			super.onBackPressed();
			finish();
		} else if (adLoaded) {
			adShown = true;
			startAppAd.showAd(new AdDisplayListener() {
				@Override
				public void adHidden(Ad arg0) {
					finish();
				}
				
				@Override
				public void adDisplayed(Ad arg0) {}
				
				@Override
				public void adClicked(Ad arg0) {
					finish();
				}
			});
		}
	}
	
	public void marketRedirect(View view){
		openMarket(this, this.getString(R.string.ownMarketURL));
	}
	
	private boolean saveBackgroundChoiceToPrefs(int position) {
		SharedPreferences.Editor editor = prefs.edit();
		editor.putInt(SPINNER_POSITION, position);
		editor.putInt(WALLPAPER_NUMBER, position);
		
		if (editor.commit()) {
			return true;
		}
		
		return false;
	}
}
